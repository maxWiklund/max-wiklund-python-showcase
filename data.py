from maya import cmds
import os
class Data:
	"""	Author. Max Wiklund
	email: info@maxWiklund.com
	date: 06-06/2018
	last update: 03-07/2018
	Copyright (C) Max Wiklund, All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited
	Proprietary and confidential
	"""

	def displaySmoothness(self):
		items=[]
		for item in cmds.ls(type='mesh'):
			if not str(cmds.displaySmoothness(item, q=True, polygonObject=True)) in ['[1L]', '[0L]']:
				items.append(item)
		return items, len(cmds.ls(type='mesh'))


	def fixDisplaySmoothness(self, list):
		for items in list:
			cmds.displaySmoothness(items, polygonObject=1)


	def history(self):
		items=[]
		for item in cmds.ls(type='mesh'):
			if not len(cmds.listHistory(item))==1:
				items.append(cmds.listRelatives(item, p=True)[0])
		return items, len(cmds.ls(type='mesh'))


	def fixHistory(self, list):
		cmds.select(list)
		cmds.DeleteHistory()
		cmds.select(list, d=True)


	def namespace(self):
		items=[]
		for item in cmds.ls(type='mesh'):
			if ':' in item:
				items.append(item)
		return items, len(cmds.ls(type='mesh'))


	def fixNamespace(self, list):
		for items in list:
			namespace=items.split(':')[0]
			try:
				cmds.namespace(removeNamespace=':'+namespace, mergeNamespaceWithParent=True)
			except:
				pass


	def renderStats(self):
		items=[]
		for item in cmds.ls(type='mesh'):
			if cmds.getAttr(item+'.visibleInReflections')==False or cmds.getAttr(item+'.visibleInRefractions')==False \
					or cmds.getAttr(item+'.primaryVisibility')==False or cmds.getAttr(item+'.castsShadows')==False \
					or cmds.getAttr(item+'.receiveShadows')==False or cmds.getAttr(item+'.smoothShading')==False:
				items.append(item)
		return items, len(cmds.ls(type='mesh'))


	def fixRenderStats(self, list):
		for items in list:
			for attributes in ['visibleInReflections', 'visibleInRefractions', 'primaryVisibility', 'castsShadows',
							   'receiveShadows', 'smoothShading']:
				cmds.setAttr(items+'.%s' %(attributes), 1)


	def refrenses(self):
		items=[]
		for refrense in cmds.file(r=True, q=True):
			items.append(refrense)
		return items, len(items)


	def fixRefrenses(self, list):
		for items in list:
			try:
				cmds.file(items, importReference=True, f=True)
			except:
				cmds.warning('Import reference failed')


	def lambertNotAssigned(self):
		items=[]
		for item in cmds.ls(type='transform'):
			try:
				if cmds.objectType(cmds.listRelatives(item, c=True)[0], isType='mesh'):
					if not 'initialShadingGroup' in cmds.listConnections(cmds.listRelatives(item, c=True)[0]):
						if not cmds.listRelatives(item, c=True)[0] in items:
							items.append(cmds.listRelatives(item, c=True)[0])
			except:
				pass
		return items, len(cmds.ls(type='mesh'))


	def fixLambertNotAssigned(self, list):
		for items in list:
			for SH in cmds.ls(type='shadingEngine'):
				cmds.sets(items, rm=SH)
			cmds.sets(items, add='initialShadingGroup')


	def transform(self):
		items=[]
		for item in cmds.ls(type='mesh'):
			transform=cmds.listRelatives(item, p=True)[0]
			for Attr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz']:
				if not cmds.getAttr('%s.%s'%(transform, Attr))==0:
					items.append(transform)
					break
		return items, len(cmds.ls(type='mesh'))


	def fixTransform(self, list):
		for items in list:
			cmds.makeIdentity(items, apply=True)


	def multipleShapes(self):
		list=[]
		for item in cmds.ls(type='mesh'):
			transform=cmds.listRelatives(item, p=True)[0]
			if not len(cmds.listRelatives(transform, c=True))==1:
				if not transform in list:
					list.append(transform)
		return list, len(cmds.ls(type='mesh'))


	def fixMultipleShapes(self, list):
		for items in list:
			for shapes in cmds.listRelatives(items):
				if cmds.listConnections(shapes)==None:
					cmds.delete(shapes)


	def origoPivot(self):
		list=[]
		for item in cmds.ls(type='mesh'):
			transform=cmds.listRelatives(item, p=True)[0]
			if cmds.xform(transform+'.scalePivot', q=True, ws=True, t=True)==[0.0, 0.0, 0.0]:
				continue
			if not transform in list:
				list.append(transform)
		return list, len(cmds.ls(type='mesh'))


	def fixOrigoPivot(self, list):
		for items in list:
			cmds.move(0, 0, 0, items+'.scalePivot', items+'.rotatePivot', absolute=True)


	def enableOverrides(self):
		list=[]
		try:
			if cmds.getAttr('model_grp.overrideEnabled')==False or cmds.getAttr('model_grp.overrideDisplayType')==0 or cmds.getAttr('model_grp.overrideDisplayType')==1:
				list.append('model_grp')
		except:
			pass
		return list, 1


	def fixEnableOverrides(self, item):
		cmds.setAttr('model_grp.overrideEnabled', 1)
		cmds.setAttr('model_grp.overrideDisplayType', 2)


	def namingConvention(self): # not implemented yet
		list=[]
		return list, 0


	def fixNamingConvention(self, list): # not implemented yet
		pass


	def itemsInModelGrp(self): # not implemented yet
		try:
			return cmds.sets('modelingPublish', q=True), 1
		except:
			return [], 0


	def findLookedGrp(self):
		list=[]
		for item in cmds.ls('*_ctrl'):
			if cmds.objectType(cmds.listRelatives(item, p=True)[0], isType='transform'):
				for Attr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v']:
					if cmds.getAttr('%s.%s'%(cmds.listRelatives(item, p=True)[0], Attr), keyable=True)==True or cmds.getAttr('%s.%s'%(cmds.listRelatives(item, p=True)[0], Attr), lock=True)==False:
						list.append(cmds.listRelatives(item, p=True)[0])
						break
		return list, len(cmds.ls('*_ctrl'))


	def fixLookedGrp(self, list):
		for selectedItems in list:
			for Attr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v']:
				try:
					cmds.setAttr('%s.%s'%(selectedItems, Attr), lock=True, keyable=False, channelBox=False)
				except:
					pass


	def controlTransforms(self):
		items=[]
		for item in cmds.ls('*_ctrl'):
			for Attr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz']:
				if cmds.getAttr('%s.%s'%(item, Attr), lock=True):
					continue
				if not cmds.getAttr('%s.%s'%(item, Attr))==0:
					items.append(item)
					break
		return items, len(cmds.ls('*_ctrl'))


	def fixControlTransforms(self, list):
		for items in list:
			for Attr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
				if cmds.getAttr('%s.%s'%(items, Attr), lock=True):
					continue
				if Attr=='sx' or Attr=='sy' or Attr=='sz':
					cmds.setAttr('%s.%s'%(items, Attr), 1)
				else:
					cmds.setAttr('%s.%s'%(items, Attr), 0)


	def controlKeyframes(self):
		items=[]
		for item in cmds.ls('*_ctrl'):
			for Attr in cmds.listAttr(item, v=True, k=True):
				if not cmds.keyframe('%s.%s'%(item, Attr), q=True)==None:
					items.append(item)
					break
		return items, len(cmds.ls('*_ctrl'))


	def fixControlKeyframes(self, list):
		for item in list:
			for Attr in cmds.listAttr(item, v=True, k=True):
				cmds.cutKey('%s.%s' %(item, Attr), s=True)


	def lambertAssigned(self):
		items=[]
		for item in cmds.ls(type='transform'):
			try:
				if cmds.objectType(cmds.listRelatives(item, c=True)[0], isType='mesh'):
					if 'initialShadingGroup' in cmds.listConnections(cmds.listRelatives(item, c=True)[0]):
						if not cmds.listRelatives(item, c=True)[0] in items:
							items.append(item)
			except:
				pass
		return items, cmds.ls(type='mesh')


	def sceneScale(self):
		if not cmds.currentUnit(query=True, linear=True)=='cm':
			return ['Fix The Scene Scale!'], 1
		else:
			return [], 0


	def renderCamera(self):
		items=[]
		for item in cmds.ls(type='camera'):
			if not 'renderCam' in item:
				if not item in ['frontShape', 'perspShape', 'sideShape', 'topShape']:
					items.append(item)
		return items, len(cmds.ls(type='camera'))

	def abcSize(self):
		fileSize=0
		for alembicFilePath in cmds.ls(type='AlembicNode'):
			if len(cmds.listConnections(alembicFilePath))>=2:
				statinfo=os.path.getsize(cmds.getAttr('%s.abc_File'%(alembicFilePath)))
				fileSize=fileSize+statinfo*10**-6

		lengt=str(fileSize).split('.')[0]
		if len(lengt)<=3:
			return str(fileSize)[:6]+' mb'
		else:
			fileSize=fileSize*0.001
			return str(fileSize)[:6]+' gb'


	def listAOVs(self):
		items=[]
		for item in cmds.ls(type='aiAOV'):
			items.append(item)
		return items


	def listRenderLayers(self):
		items=[]
		for item in cmds.ls(type='renderLayer'):
			if not ':' in item:
				items.append(item)
		return items


	def listLightsSampels(self):
		items=[]
		for light in ['ambientLight', 'directionalLight', 'pointLight', 'spotLight', 'areaLight', 'volumeLight',
					  'aiAreaLight', 'aiSkyDomeLight', 'aiPhotometricLight', 'aiSky']:
			for lights in cmds.ls(type=light):
				if cmds.getAttr('%s.aiSamples'%(lights))>=1:
					continue
				items.append(lights)
		return items


	def fixLightsSampels(self, list, value):
		for item in list:
			cmds.setAttr('%s.aiSamples' %(item), value)


	def pubSet(self, set):
		if cmds.objExists(set):
			if cmds.sets(set, q=True)==None:
				return ['No items in Publish-set'], 0
			else:
				return [], 0
		else:
			return ['No Publish-set found'], 0


	def listSubdiv(self):
		items=[]
		for item in cmds.ls(type='mesh'):
			if cmds.getAttr('%s.aiSubdivType' %(item))==1 and cmds.getAttr('%s.aiSubdivIterations' %(item)) >=3:
				continue
			items.append(item)
		return items


	def fixSubdiv(self, list, value):
		for items in list:
			cmds.setAttr('%s.aiSubdivType' %(items), 1)
			cmds.setAttr('%s.aiSubdivIterations'%(items), value)


class Path:
	def getImagePath(self):
		path=os.path.realpath(__file__)
		path=path.replace('\\', '/')
		return path.replace(path.split('/')[-1], '')

import json
import os
import webbrowser
import mtoa.utils as utils
import __main__
from maya import cmds
from maya import OpenMayaUI
from PySide2 import QtCore
from PySide2 import QtGui
from PySide2 import QtWidgets
from shiboken2 import wrapInstance

__main__.activeComponentPath='path/....' # From Shotgun or Ftrack

def maya_main_window():
	main_window_ptr=OpenMayaUI.MQtUtil.mainWindow()
	return wrapInstance(long(main_window_ptr), QtWidgets.QWidget)


class LightPublishUI(QtWidgets.QDialog):
	"""	Author. Max Wiklund
	email: info@maxWiklund.com
	date: 15-07/2018
	last update: 19-015/2018
	Version: 1.0
	Copyright (C) Max Wiklund, All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited
	Proprietary and confidential

	Requirements: mtoa 5.0, PySide2

	Instructions: select the lights you want to publish in the qlistWidget and click on the publish button.
	If you want to import lights just click on the import button. Custom imports and exports are also available thru
	the right click menu.
	"""


	def __init__(self, parent=maya_main_window()):
		super(LightPublishUI, self).__init__(parent)
		self.setWindowTitle('Publish Lights Window')
		self.resize(300, 450)
		self.setWindowFlags(QtCore.Qt.Tool)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
		self.creatLayout()
		self.lastSavedFile=''


	def creatLayout(self):
		self.verticalLayout=QtWidgets.QVBoxLayout()
		self.lights_listWidget=QtWidgets.QListWidget()
		self.lights_listWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
		self.addList()
		self.lights_listWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.lights_listWidget.customContextMenuRequested.connect(self.openMenu)
		self.verticalLayout.addWidget(self.lights_listWidget)
		self.horizontalLayout=QtWidgets.QHBoxLayout()
		self.import_pushButton=QtWidgets.QPushButton('Import')
		self.import_pushButton.setStyleSheet('background-color: rgb(0, 179, 255)')
		self.import_pushButton.clicked.connect(self.importLights)
		self.import_pushButton.setMinimumSize(0, 30)
		icon=QtGui.QIcon()
		icon.addPixmap(QtGui.QPixmap('images/import.png'), QtGui.QIcon.Normal, QtGui.QIcon.Off)
		self.import_pushButton.setIcon(icon)
		self.horizontalLayout.addWidget(self.import_pushButton)
		self.publish_pushButton=QtWidgets.QPushButton('Publish')
		self.publish_pushButton.setMinimumSize(0, 30)
		icon=QtGui.QIcon()
		icon.addPixmap(QtGui.QPixmap('images/publish.png'), QtGui.QIcon.Normal, QtGui.QIcon.Off)
		self.publish_pushButton.setIcon(icon)
		self.publish_pushButton.clicked.connect(self.exportLights)
		self.publish_pushButton.setStyleSheet('background-color: rgb(0, 179, 255)')
		self.horizontalLayout.addWidget(self.publish_pushButton)
		self.verticalLayout.addLayout(self.horizontalLayout)
		self.setLayout(self.verticalLayout)


	def addList(self):
		""" Adding and refreshing the light list """
		self.lights_listWidget.clear()
		for light in 'ambientLight', 'directionalLight', 'pointLight', 'spotLight', 'areaLight', 'volumeLight', \
					 'aiAreaLight', 'aiSkyDomeLight', 'aiPhotometricLight', 'aiSky':
			for lights in cmds.ls(type=light):
				item=QtWidgets.QListWidgetItem(cmds.listRelatives(lights, p=True)[0])
				item.setSizeHint(QtCore.QSize(0, 30))
				self.lights_listWidget.addItem(item)


	def getLightType(self, lightType):
		""" Find light type """
		for light in 'areaLight', 'pointLight', 'directionalLight', 'spotLight', 'ambientLight', 'volumeLight', \
					 'aiAreaLight', 'aiSkyDomeLight', 'aiPhotometricLight':
			if cmds.objectType(lightType, isType=light):
				return light


	def exportLights(self, customPath=False, dirPath=''):
		""" Export selected lights from the GUI. """
		exportPath=__main__.activeComponentPath
		if customPath:
			exportPath=dirPath
		exportLights={}
		for selectedItem in self.lights_listWidget.selectedItems():
			selectedItem=unicode(selectedItem.text())
			lightShape=cmds.listRelatives(selectedItem, c=True)[0]
			if cmds.listConnections('%s.color'%(lightShape))==None:  # Get Textur path
				texturePath=''
				texture=False
			else:
				texturePath=cmds.getAttr('%s.fileTextureName'%(cmds.listConnections('%s.color'%(lightShape))[0]))
				texture=True
			try:
				Exposure=cmds.getAttr('%s.exposure'%(lightShape)),
			except:
				Exposure=cmds.getAttr('%s.aiExposure'%(lightShape)),
			exportLights[selectedItem]={
				'lightType': self.getLightType(lightShape),
				'name': selectedItem,
				'Translate': cmds.xform(selectedItem, ws=True, t=True, q=True),
				'Rotate': cmds.xform(selectedItem, ws=True, ro=True, q=True),
				'Scale': cmds.xform(selectedItem, ws=True, s=True, q=True),
				'Color': cmds.getAttr('%s.color'%(lightShape)),
				'TexturePath': texturePath,
				'lightTexture_bol': texture,
				'Intensity': cmds.getAttr('%s.intensity'%(lightShape)),
				'Exposure': Exposure,
				'aiUseColorTemperature': cmds.getAttr('%s.aiUseColorTemperature'%(lightShape)),
				'aiColorTemperature': cmds.getAttr('%s.aiColorTemperature'%(lightShape)),
				'aiSamples': cmds.getAttr('%s.aiSamples'%(lightShape)),
				'aiNormalize': cmds.getAttr('%s.aiNormalize'%(lightShape)),
				'aiCastShadows': cmds.getAttr('%s.aiCastShadows'%(lightShape)),
				'aiShadowDensity': cmds.getAttr('%s.aiShadowDensity'%(lightShape)),
				'aiDiffuse': cmds.getAttr('%s.aiDiffuse'%(lightShape)),
				'aiSpecular': cmds.getAttr('%s.aiSpecular'%(lightShape)),
				'aiSss': cmds.getAttr('%s.aiSss'%(lightShape)),
				'aiIndirect': cmds.getAttr('%s.aiIndirect'%(lightShape)),
				'aiVolume': cmds.getAttr('%s.aiVolume'%(lightShape)),
				'lightGroup': cmds.getAttr('%s.aiAov' %(lightShape))
			}

		convertedJsonFile=json.dumps(exportLights, indent=4)
		with open('%s/lights.json'%(exportPath), 'w') as outfile:
			outfile.write(convertedJsonFile)
		self.lastSavedFile='%s/lights.json'%(exportPath)

	def importLights(self, customPath=False, dirPath=''):
		importPath='%s/lights.json'%(__main__.activeComponentPath)
		if customPath:
			importPath=dirPath
		if not os.path.exists(importPath):
			cmds.warning('Light path dos not exists')
			return
		with open(importPath) as f:
			exportedInformation=f.read()
		data=json.loads(exportedInformation)
		for lights in data:
			if data[lights]['lightType'] in ['aiAreaLight', 'aiSkyDomeLight', 'aiPhotometricLight']:
				light=utils.createLocator(data[lights]['lightType'], asLight=True)
			else:
				lght=cmds.shadingNode(data[lights]['lightType'], asLight=True)
				light=[cmds.listRelatives(lght, c=True)[0], lght]
			cmds.xform(light[1], t=data[lights]['Translate'], ro=data[lights]['Rotate'], s=data[lights]['Scale'])
			cmds.setAttr(light[0]+'.intensity', data[lights]['Intensity'])
			try:
				cmds.setAttr(light[0]+'.exposure', data[lights]['Exposure'])
			except:
				cmds.setAttr(light[0]+'.aiExposure', data[lights]['Exposure'][0])
			for attributes in ['aiUseColorTemperature', 'aiColorTemperature', 'aiSamples', 'aiNormalize',
							   'aiCastShadows', 'aiShadowDensity', 'aiDiffuse', 'aiSpecular', 'aiSss', 'aiIndirect',
							   'aiVolume']:
				cmds.setAttr(light[0]+'.'+attributes, data[lights][attributes])

			if data[lights]['lightTexture_bol']==True:
				imageFil=cmds.shadingNode('file', asTexture=True, icm=True)
				cmds.setAttr(imageFil+'.fileTextureName', data[lights]['TexturePath'], type='string')
				cmds.connectAttr(imageFil+'.outColor', light[0]+'.color', f=True)
			else:
				cmds.setAttr(light[0]+'.color', data[lights]['Color'][0][0], data[lights]['Color'][0][1],
							 data[lights]['Color'][0][2], type='double3')
			cmds.setAttr(light[0]+'.aiAov', data[lights]['lightGroup'], type='string')
			self.addList()


	def openMenu(self, position):
		""" Add a right click menu for the QListWidget"""
		menu=QtWidgets.QMenu()
		refresh=menu.addAction('Refresh List')
		remove=menu.addAction('Remove item')
		menu.addSeparator()
		openFolder=menu.addAction('Open In Explorer')
		exportSelection=menu.addAction('Custom Export')
		importCustomFiel=menu.addAction('Custom Import')
		action=menu.exec_(self.lights_listWidget.mapToGlobal(position))
		if action==refresh:
			self.addList()  # Refresh List
		elif action==remove:
			self.lights_listWidget.takeItem(self.lights_listWidget.currentRow())
		elif action==openFolder:
			self.openFolder()
		elif action==exportSelection:
			self.customExport()
		elif action==importCustomFiel:
			self.customImport()


	def keyPressEvent(self, event):
		""" Handels Key press events"""
		key=event.key()
		if key==QtCore.Qt.Key_Delete:
			self.lights_listWidget.takeItem(self.lights_listWidget.currentRow())
		elif key==QtCore.Qt.Key_F5:
			self.addList()

	def customExport(self):
		folderPath=QtWidgets.QFileDialog.getExistingDirectory(None, 'Select Folder')
		if folderPath=='':
			return
		else:
			self.exportLights(True, folderPath)


	def customImport(self):
		fileDialog=QtWidgets.QFileDialog()
		file=fileDialog.getOpenFileName(None, 'Import Lights From json file', '', 'JSON (*.json)')
		if not file==(u'', u''):
			self.importLights(True, file[0])


	def openFolder(self):
		if not self.lastSavedFile=='':
			webbrowser.open(self.lastSavedFile)


if __name__=='__main__':
	try:
		window.close()
	except:
		pass
	window=LightPublishUI()
	window.show()
import os
from maya import cmds
import data
reload(data)

class PublishTypes(object):
	animationPublish='animationPublish'
	groomPublish='groomPublish'
	modelingPublish='modelingPublish'
	lookDevPublish='lookDevPublish'
	rigPublish='rigPublish'
	techAnimPublish='techAnimPublish'
	matchmovePublish='matchmovePublish'
	lightingPublish='lightingPublish'
	renderPublish='renderPublish'


class CheckTypes(object):
	PopulatedPublisher='Populated_Publisher'
	#model
	History='History'
	Namespace='Namespace'
	Naming_Convention='Naming_Convention'
	RenderStats='RenderStats'
	Refrenses='Refrenses'
	Lambert1_Not_Assigned='Lambert1_Not_Assigned'
	OrigoPivot='Origo_Pivot'
	Transform='Transform'
	Multiple_Shapes='Multiple_Shapes'
	workinUnits='Workin_Units_In_cm'
	# Rig
	DisplaySmoothness='Display_Smoothness'
	EnableOverrides='Enable_Overrides_On_model_grp'
	ControlTransforms='Control_Transformation'
	ControlKeyframes='Control_Keyframes'
	NonLockedGroups='Non_Locked_Groups'
	# Lookdev
	UnassignedShaders='Unassigned_Shaders'
	lambertAssigned='Lambert1_Assigned'
	# Rendering
	Subdiv='subdiv'
	LightSampels='Light_Sampels'
	RenderCamera='renderCamera'
	AOVs='AOVs'
	RenderLayers='RenderLayers'
	Lights='Lights'
	Fix='Fix'
	FixAll='FixAll'


class SceneChecker:
	"""	Author. Max Wiklund
	email: info@maxWiklund.com
	date: 02-07/2018
	last update: 03-07/2018
	Copyright (C) Max Wiklund, All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited
	Proprietary and confidential
	"""
	def __init__(self, PublishSets):
		if cmds.window('sceneChecker_window', q=True, exists=True):
			cmds.deleteUI('sceneChecker_window')
		cmds.loadUI(uiFile=data.Path().getImagePath()+'sceneChecker.ui')
		cmds.button('ReTest_pushButton', e=True, c=lambda *_:self.startChek(PublishSets, False))
		cmds.showWindow('sceneChecker_window')
		parentLayout=cmds.button('renderCheckerOveral_pushButton', q=True, p=True)
		cmds.menuItem('Documentation_action', e=True, c="""import webbrowser; webbrowser.open('https://bitbucket.org/maxWiklund/max-wiklund-python-showcase/wiki/Home')""")
		self.baseLayout=cmds.tabLayout(parent=parentLayout)
		cmds.deleteUI('renderCheckerOveral_pushButton')
		imageButton=cmds.button('image_pushButton', q=True, p=True)
		cmds.picture('greade', image=data.Path().getImagePath()+'images/F.png', p=imageButton)
		cmds.deleteUI('image_pushButton')

		self.startChek(PublishSets, True)


	def startChek(self, PublishSets, firstTime):
		self.totalChekcs=[] # count
		self.totalProblems=0

		if firstTime==False:
			self.lastUsedTab=cmds.tabLayout(self.baseLayout, q=True, selectTab=True)
			for tablayouts in PublishSets:
				try:
					cmds.deleteUI(tablayouts)
				except:
					pass

		for sets in PublishSets:
			layout=cmds.columnLayout(sets, parent=self.baseLayout, adj=True)
			cmds.separator(sets, p=layout)
			# Setup of tasks
			self.listProblems=[]

			if PublishTypes().modelingPublish==sets:
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.Namespace, data.Data().namespace())
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.Naming_Convention, data.Data().namingConvention())
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.RenderStats, data.Data().renderStats())
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.workinUnits, data.Data().sceneScale())
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.History, data.Data().history())
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.Refrenses, data.Data().refrenses())
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.Lambert1_Not_Assigned, data.Data().lambertNotAssigned())
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.OrigoPivot, data.Data().origoPivot())
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.Transform, data.Data().transform())
				self.creatFrameInfo(PublishTypes.modelingPublish, CheckTypes.Multiple_Shapes, data.Data().multipleShapes())
				cmds.tabLayout(self.baseLayout, e=True, tabLabel=[sets, 'Model -'+self.countErors(self.listProblems)])# Rename Tab

			elif PublishTypes().rigPublish==sets:
				self.creatFrameInfo(PublishTypes.rigPublish, CheckTypes.ControlTransforms, data.Data().controlTransforms())
				self.creatFrameInfo(PublishTypes.rigPublish, CheckTypes.NonLockedGroups, data.Data().findLookedGrp())
				self.creatFrameInfo(PublishTypes.rigPublish, CheckTypes.DisplaySmoothness, data.Data().displaySmoothness())
				self.creatFrameInfo(PublishTypes.rigPublish, CheckTypes.ControlKeyframes, data.Data().controlKeyframes())
				self.creatFrameInfo(PublishTypes.rigPublish, CheckTypes.EnableOverrides, data.Data().enableOverrides())
				cmds.tabLayout(self.baseLayout, e=True, tabLabel=[sets, 'Rig -'+self.countErors(self.listProblems)])

			elif PublishTypes().lookDevPublish==sets:
				self.creatFrameInfo(PublishTypes.lookDevPublish, CheckTypes.UnassignedShaders, data.Data().lambertAssigned())
				cmds.tabLayout(self.baseLayout, e=True, tabLabel=[sets, 'Lookdev -'+self.countErors(self.listProblems)])

			elif PublishTypes().lightingPublish==sets:
				self.creatFrameInfo(PublishTypes.lightingPublish, CheckTypes.lambertAssigned, data.Data().lambertAssigned())
				cmds.tabLayout(self.baseLayout, e=True, tabLabel=[sets, 'Lighting -'+self.countErors(self.listProblems)])

			elif PublishTypes().renderPublish==sets:
				self.creatFrameInfo(PublishTypes.renderPublish, CheckTypes.RenderCamera, data.Data().renderCamera())
				rendersettings=cmds.loadUI(uiFile=data.Path().getImagePath()+'renderSettings.ui')
				cmds.control(rendersettings, e=True, p=PublishTypes().renderPublish)
				cmds.textField('imageWith_lineEdit', e=True, tx=cmds.getAttr('defaultResolution.width'))
				cmds.textField('imageHeight_lineEdit', e=True, tx=cmds.getAttr('defaultResolution.height'))
				cmds.textField('startFrame_lineEdit', e=True, tx=cmds.getAttr('defaultRenderGlobals.startFrame'))
				cmds.textField('endFrame_lineEdit', e=True, tx=cmds.getAttr('defaultRenderGlobals.endFrame'))
				cmds.textField('AASamples_lineEdit', e=True, tx=cmds.getAttr('defaultArnoldRenderOptions.AASamples'))
				if cmds.getAttr('defaultArnoldRenderOptions.motion_blur_enable'):
					cmds.checkBox('motionBlur_checkBox', e=True, l='On', v=True)
				cmds.text('abcSize_label', e=True, l='Abc size '+data.Data().abcSize())

				self.renderSettings(CheckTypes().Subdiv, data.Data().listSubdiv())
				self.renderSettings(CheckTypes().LightSampels, data.Data().listLightsSampels())
				cmds.tabLayout(self.baseLayout, e=True, tabLabel=[sets, 'Render -'+self.countErors(self.listProblems)])


		percentage=(float(self.problems(self.totalChekcs))-self.totalProblems)/self.problems(self.totalChekcs)*100
		cmds.text('score_label', e=True, l=str(percentage)[:5]+' %')
		cmds.text('totalErors_label', e=True,l=self.totalProblems)
		cmds.text('totalChecks_label', e=True, l=self.problems(self.totalChekcs))

		if percentage==100:
			cmds.picture('greade', image=data.Path().getImagePath()+'images/A+.png', e=True)
		elif percentage>=95:
			cmds.picture('greade', image=data.Path().getImagePath()+'images/A.png', e=True)
		elif percentage>=90:
			cmds.picture('greade', image=data.Path().getImagePath()+'images/B+.png', e=True)
		elif percentage>=85:
			cmds.picture('greade', image=data.Path().getImagePath()+'images/B.png', e=True)
		elif percentage>=80:
			cmds.picture('greade', image=data.Path().getImagePath()+'images/C+.png', e=True)
		elif percentage>=75:
			cmds.picture('greade', image=data.Path().getImagePath()+'images/C.png', e=True)
		elif percentage>=70:
			cmds.picture('greade', image=data.Path().getImagePath()+'images/D.png', e=True)
		else:
			cmds.picture('greade', image=data.Path().getImagePath()+'images/F.png', e=True)

		if firstTime==False:# open last Tab
			cmds.tabLayout(self.baseLayout, e=True, selectTab=self.lastUsedTab)


	def creatFrameInfo(self, tabName, task, list):
		""" Framelayout GUI"""
		layout=cmds.frameLayout(task, label=task+' -0', cll=True, p=cmds.separator(tabName, p=True, q=True), bgc=[0.1, 0.5, 0.0], cl=False)

		self.listProblems.append(str(len(list[0])))
		self.totalChekcs.append(list[1])
		if not list[0]==[]:
			rowLayout=cmds.rowLayout(task, numberOfColumns=14, p=layout)
			self.addButtons(rowLayout, task)
			cmds.textScrollList(task, p=layout, ra=True, ams=True)
			for item in list[0]:
				cmds.textScrollList(task, e=True, a=item)
			cmds.frameLayout(task, e=True, label=task+' -'+str(len(list[0])), bgc=[0.9, 0.1, 0.0])



	def addButtons(self, layout, layoutNmae):
		"""Creating buttons for the frameLayout"""
		comandDeselect=cmds.button(layoutNmae+'_Deselect', l='Deselect', p=layout, w=70, c=lambda*_:self.scrollList(comandDeselect, layoutNmae))
		comandSelect=cmds.button(layoutNmae+'_Select', l='Select', p=layout, w=70, c=lambda*_: self.scrollList(comandSelect, layoutNmae))
		comandSelectAll=cmds.button(layoutNmae+'_SelectAll', l='Select All', p=layout, w=70, c=lambda*_:self.scrollList(comandSelectAll, layoutNmae))
		comandFix=cmds.button(layoutNmae+'_Fix', l='Fix', p=layout, w=70, c=lambda*_:self.sendComand(comandFix, layoutNmae))
		comandFixAll=cmds.button(layoutNmae+'_FixAll', l='Fix All', p=layout, w=70, c=lambda*_:self.sendComand(comandFixAll, layoutNmae))



	def renderSettings(self, layoutNmae, list):
		""" Setting up render buttons commands"""
		comandDeselect=cmds.button(layoutNmae+'_Deselect', l='Deselect', e=True, w=70, c=lambda *_: self.scrollList(comandDeselect, layoutNmae))
		comandFix=cmds.button(layoutNmae+'_Fix', l='Fix', e=True, w=70, c=lambda *_: self.sendComand(comandFix, layoutNmae))
		comandFixAll=cmds.button(layoutNmae+'_FixAll', l='Fix All', e=True, w=70, c=lambda *_: self.sendComand(comandFixAll, layoutNmae))
		cmds.textScrollList(layoutNmae, e=True, ra=True, ams=True)
		for item in list:
			cmds.textScrollList(layoutNmae, e=True, a=item)


	def countErors(self, list):
		if list==[]:
			return '0'
		else:
			value='+'.join(list)
			count=eval(value)
			self.totalProblems+=count
			return str(count)


	def problems(self, list):
		value=0
		for items in list:
			value+=items
		return value


	def sendComand(self, command, layoutNmae):
		if command.split('_')[-1]=='Fix':
			if not cmds.textScrollList(layoutNmae, si=True, q=True)==[]:
				self.fixOrFixAll(cmds.textScrollList(layoutNmae, si=True, q=True), layoutNmae)
			else:
				cmds.warning('select somthing')
		elif  command.split('_')[-1]=='FixAll':
			self.fixOrFixAll(cmds.textScrollList(layoutNmae, ai=True, q=True), layoutNmae)


	def scrollList(self, command, layoutNmae):
		""" Select or deselect objects in list """
		if command.split('_')[-1]=='Deselect':
			cmds.select(cmds.textScrollList(layoutNmae, ai=True, q=True), d=True)
			for items in cmds.textScrollList(layoutNmae, ai=True, q=True):
				cmds.textScrollList(layoutNmae, e=True, di=items)
		elif command.split('_')[-1]=='Select':
			cmds.select(cmds.textScrollList(layoutNmae, si=True, q=True))
		elif command.split('_')[-1]=='SelectAll':
			cmds.select(cmds.textScrollList(layoutNmae, ai=True, q=True))
			for items in cmds.textScrollList(layoutNmae, ai=True, q=True):
				cmds.textScrollList(layoutNmae, e=True, si=items)


	def fixOrFixAll(self, list, layoutNmae):
		if layoutNmae==CheckTypes().DisplaySmoothness:
			data.Data().fixDisplaySmoothness(list)

		elif layoutNmae==CheckTypes().History:
			data.Data().fixHistory(list)

		elif layoutNmae==CheckTypes().Namespace:
			data.Data().fixNamespace(list)

		elif layoutNmae==CheckTypes().RenderStats:
			data.Data().fixRenderStats(list)

		elif layoutNmae==CheckTypes().Refrenses:
			data.Data().fixRefrenses(list)

		elif layoutNmae==CheckTypes().Lambert1_Not_Assigned:
			data.Data().fixLambertNotAssigned(list)

		elif layoutNmae==CheckTypes().Transform:
			data.Data().fixTransform(list)

		elif layoutNmae==CheckTypes().OrigoPivot:
			data.Data().fixOrigoPivot(list)

		elif layoutNmae==CheckTypes().EnableOverrides:
			data.Data().fixEnableOverrides(list)

		elif layoutNmae==CheckTypes().Multiple_Shapes:
			data.Data().fixMultipleShapes(list)

		elif layoutNmae==CheckTypes().ControlTransforms:
			data.Data().fixControlTransforms(list)

		elif layoutNmae==CheckTypes().NonLockedGroups:
			data.Data().fixLookedGrp(list)

		elif layoutNmae==CheckTypes().ControlKeyframes:
			data.Data().fixControlKeyframes(list)

		elif layoutNmae==CheckTypes().Subdiv:
			data.Data().fixSubdiv(list, cmds.intSlider('subdiv_horizontalSlider', q=True, v=True))

		elif layoutNmae==CheckTypes().LightSampels:
			data.Data().fixLightsSampels(list, cmds.intSlider('Light_Sampels_horizontalSlider', q=True, v=True))
		else:
			pass


SceneChecker(['modelingPublish','rigPublish'])